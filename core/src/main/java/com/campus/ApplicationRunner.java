package com.campus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan("com.campus.domain")
@EnableJpaRepositories("com.campus.repos")
@SpringBootApplication(scanBasePackages = "com.campus")
public class ApplicationRunner {

    public static void main(final String[] args) {
        SpringApplication.run(ApplicationRunner.class, args);
    }
}
